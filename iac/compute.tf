resource "azurerm_linux_virtual_machine" "bastion-vm" {
  name                  = "${var.name-prefix}bh"
  resource_group_name   = data.azurerm_resource_group.main-rc.name
  location              = data.azurerm_resource_group.main-rc.location
  size                  = "Standard_B1s"
  admin_username        = "bastionadmin"
  network_interface_ids = [azurerm_network_interface.bastion-nic.id]


  user_data = base64encode(file("init/init-bastion-host.sh"))

  admin_ssh_key {
    username   = "bastionadmin"
    public_key = file("ssh/bastion_rsa.pub")
  }

  source_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "18.04-LTS"
    version   = "latest"
  }

  os_disk {
    storage_account_type = "Standard_LRS"
    caching              = "ReadWrite"
    disk_size_gb         = "32"
  }
}


resource "azurerm_linux_virtual_machine" "gluster-vm" {
  count               = var.cluster-size
  name                = "${var.name-prefix}gluster0${count.index + 1}"
  resource_group_name = data.azurerm_resource_group.main-rc.name
  location            = data.azurerm_resource_group.main-rc.location
  size                = "Standard_B1s"
  admin_username      = "glusteradmin"
  network_interface_ids = [
    azurerm_network_interface.gluster-nic[count.index].id,
  ]


  user_data = base64encode(file("init/init-gluster-server.sh"))

  admin_ssh_key {
    username   = "glusteradmin"
    public_key = file("ssh/ansible_rsa.pub")
  }

  source_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "18.04-LTS"
    version   = "latest"
  }

  os_disk {
    storage_account_type = "Standard_LRS"
    caching              = "ReadWrite"
    disk_size_gb         = "32"
  }
}
